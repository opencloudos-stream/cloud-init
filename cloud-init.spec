Summary:        Cloud instance init scripts
Name:           cloud-init
Version:        23.2.1
Release:        11%{?dist}
License:        ASL 2.0 or GPLv3
URL:            https://github.com/canonical/cloud-init
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
Source1:        cloud-init-tmpfiles.conf

Patch5000:      Support-TencentCloud.patch


BuildArch:      noarch
BuildRequires:  pkgconfig(systemd) systemd-rpm-macros systemd
BuildRequires:  python3-devel python3-setuptools

BuildRequires:  python3-pytest python3-pytest-mock
BuildRequires:  iproute passwd python3-configobj
BuildRequires:  python3-distro python3-httpretty >= 0.8.14-2
BuildRequires:  python3-jinja2 python3-jsonpatch
BuildRequires:  python3-jsonschema
BuildRequires:  python3-oauthlib python3-prettytable
BuildRequires:  python3-pyserial python3-PyYAML
BuildRequires:  python3-requests python3-responses
BuildRequires:  python3-six python3-netifaces
BuildRequires:  /usr/bin/dnf

Requires:       dhcp-client hostname
Requires:       e2fsprogs iproute
Requires:       python3-libselinux net-tools
Requires:       policycoreutils-python3 procps
Requires:       python3-configobj python3-distro
Requires:       python3-jinja2 python3-oauthlib
Requires:       python3-jsonpatch python3-jsonschema
Requires:       python3-prettytable python3-pyserial
Requires:       python3-PyYAML python3-requests
Requires:       python3-six shadow-utils
Requires:       util-linux xfsprogs
Requires:       gdisk openssl

%{?systemd_requires}


%description
Cloud-init is a set of init scripts for cloud instances.  Cloud instances
need special scripts to run during initialization to retrieve and install
ssh keys and to let the user run various scripts.


%prep
%autosetup -p1
sed -i -e 's|#!/usr/bin/env python|#!/usr/bin/env python3|' \
       -e 's|#!/usr/bin/python|#!/usr/bin/python3|' tools/* cloudinit/ssh_util.py
sed -i -e 's|#!/usr/bin/python||' cloudinit/cmd/main.py

sed -i 's/"\\o"/"\/o"/' setup.py

find tests/ -type f | xargs sed -i s/unittest2/unittest/
find tests/ -type f | xargs sed -i s/assertItemsEqual/assertCountEqual/

%build
%py3_build

%install
%py3_install -- --init-system=systemd
python3 tools/render-cloudcfg --variant %{_vendor} > %{buildroot}/%{_sysconfdir}/cloud/cloud.cfg

mkdir -p %{buildroot}/var/lib/cloud
mkdir -p %{buildroot}/run/cloud-init
mkdir -p %{buildroot}/%{_tmpfilesdir}
cp -p %{SOURCE1} %{buildroot}/%{_tmpfilesdir}/%{name}.conf

mkdir -p %{buildroot}/%{_sysconfdir}/rsyslog.d
cp -p tools/21-cloudinit.conf %{buildroot}/%{_sysconfdir}/rsyslog.d/21-cloudinit.conf

[ ! -d %{buildroot}/usr/lib/systemd/system-generators ] && mkdir -p %{buildroot}/usr/lib/systemd/system-generators
python3 tools/render-cloudcfg --variant %{_vendor} systemd/cloud-init-generator.tmpl > %{buildroot}/usr/lib/systemd/system-generators/cloud-init-generator
chmod 755 %{buildroot}/usr/lib/systemd/system-generators/cloud-init-generator

mkdir -p %{buildroot}%{_mandir}/man1/
for man in cloud-id.1 cloud-init.1 cloud-init-per.1; do
    install -c -m 0644 doc/man/${man} %{buildroot}%{_mandir}/man1/${man}
    chmod -x %{buildroot}%{_mandir}/man1/*
done

cp -a %{buildroot}/etc/systemd %{buildroot}/usr/lib
rm -rf %{buildroot}/etc/systemd

%check
python3 -m pytest tests/unittests

%post
%systemd_post cloud-config.service cloud-config.target cloud-final.service cloud-init.service cloud-init.target cloud-init-local.service
if [ $1 -eq 1 ] ; then
    /bin/systemctl enable cloud-config.service     >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-final.service      >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-init.service       >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-init-local.service >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-init.target        >/dev/null 2>&1 || :
elif [ $1 -eq 2 ]; then
    /bin/systemctl is-enabled cloud-config.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-config.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-final.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-final.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-init.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-init.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-init-local.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-init-local.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-init.target >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-init.target >/dev/null 2>&1 || :

fi

%preun
%systemd_preun cloud-config.service cloud-config.target cloud-final.service cloud-init.service cloud-init.target cloud-init-local.service
if [ $1 -eq 0 ] ; then
    /bin/systemctl --no-reload disable cloud-config.service >/dev/null 2>&1 || :
    /bin/systemctl --no-reload disable cloud-final.service  >/dev/null 2>&1 || :
    /bin/systemctl --no-reload disable cloud-init.service   >/dev/null 2>&1 || :
    /bin/systemctl --no-reload disable cloud-init-local.service >/dev/null 2>&1 || :
    /bin/systemctl --no-reload disable cloud-init.target     >/dev/null 2>&1 || :
fi


%postun
%systemd_postun cloud-config.service cloud-config.target cloud-final.service cloud-init.service cloud-init.target cloud-init-local.service

%files
%license LICENSE LICENSE-Apache2.0 LICENSE-GPLv3
%doc ChangeLog
%doc doc/*
%doc %{_sysconfdir}/cloud/clean.d/README
%{_mandir}/man1/*
%config(noreplace) %{_sysconfdir}/cloud/cloud.cfg
%dir               %{_sysconfdir}/cloud/cloud.cfg.d
%config(noreplace) %{_sysconfdir}/cloud/cloud.cfg.d/*.cfg
%doc               %{_sysconfdir}/cloud/cloud.cfg.d/README
%dir               %{_sysconfdir}/cloud/templates
%config(noreplace) %{_sysconfdir}/cloud/templates/*
%dir               %{_sysconfdir}/rsyslog.d
%config(noreplace) %{_sysconfdir}/rsyslog.d/21-cloudinit.conf
%{_udevrulesdir}/66-azure-ephemeral.rules
%{_unitdir}/cloud-config.service
%{_unitdir}/cloud-final.service
%{_unitdir}/cloud-init.service
%{_unitdir}/cloud-init-local.service
%{_unitdir}/cloud-config.target
%{_unitdir}/cloud-init.target
/usr/lib/systemd/system-generators/cloud-init-generator
%{_unitdir}/cloud-init-hotplugd.service
%{_unitdir}/cloud-init-hotplugd.socket
%{_unitdir}/sshd-keygen@.service.d/disable-sshd-keygen-if-cloud-init-active.conf
%{_tmpfilesdir}/%{name}.conf
%{python3_sitelib}/*
%{_bindir}/cloud-init*
%{_bindir}/cloud-id
/usr/lib/%{name}
%dir /run/cloud-init
%dir /var/lib/cloud
%{_datadir}/bash-completion/completions/cloud-init


%changelog
* Thu Dec 26 2024 Feng Weiyao <wynnfeng@tencent.com> - 23.2.1-11
- [type] bugfix
- [desc] fix password set failure in Support-TencentCloud.patch

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.2.1-10
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Mon Sep 2 2024 Feng Weiyao <wynnfeng@tencent.com> - 23.2.1-9
- [type] other
- [desc] remove unused python3-tox in BuildRequires

* Mon Aug 19 2024 Feng Weiyao <wynnfeng@tencent.com> - 23.2.1-8
- [type] bugfix
- [desc] fix problem at config set in Support-TencentCloud.patch

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.2.1-7
- Rebuilt for loongarch release

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.2.1-6
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.2.1-5
- Rebuilt for OpenCloudOS Stream 23.09

* Tue Jul 04 2023 Feng Weiyao <wynnfeng@tencent.com> - 23.2.1-4
- fix bugs on BMSA2

* Tue Jul 04 2023 Feng Weiyao <wynnfeng@tencent.com> - 23.2.1-3
- rm _vendor definition in spec

* Tue Jul 04 2023 Feng Weiyao <wynnfeng@tencent.com> - 23.2.1-2
- fix the service names of Chrony and NTP to ensure remote passwd change success

* Mon Jul 03 2023 kianli <kianli@tencent.com> - 23.2.1-1
- Upgrade to 23.2.1

* Wed Jun 28 2023 Feng Weiyao <wynnfeng@tencent.com> - 23.1.2-3
- fix bugs about TencentOS in Support-TencentCloud.patch

* Wed May 17 2023 Feng Weiyao <wynnfeng@tencent.com> - 23.1.2-2
- add patch to support TencentCloud

* Wed May 17 2023 Feng Weiyao <wynnfeng@tencent.com> - 23.1.2-1
- update to 23.1.2 to support OpenCloudOS

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 22.2-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 22.2-2
- Rebuilt for OpenCloudOS Stream 23

* Wed Dec 14 2022 Feng Weiyao <wynnfeng@tencent.com> - 22.2-1
- package init.

